#!/bin/bash

cd "$(dirname "$0")"
source lib/bash_functions.sh

if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
    echo "Usage: $(basename "$0") [-h]"
    echo ""
    echo "Scrapes CoMe pad contents into new, corresponding kanthaus.online page"
    echo ""
    echo "optional arguments:"
    echo "  -h, --help            show this help message and exit"
    exit 0
fi

sleep 0.5
echo "WARNING: publishes pad contents as-is - ensure sensitive info sanitized"
sleep 0.5
echo "INFO: uses the YAML date in CoMe pad - please ensure it's correct"
sleep 0.5

echo -n "Continue? (y/n)"
read -r answer

if [ "$answer" != "${answer#[Yy]}" ]; then

  # Identify path in which this script is found
  scriptPath=$(dirname "${BASH_SOURCE[0]}")
  
  # Identify kanthaus.online repos path 
  repoPath=$(_identifyBasePath "kanthaus/kanthaus.online")

  echo "Curling pad contents (https://pad.kanthaus.online/come/download)..."
  curl https://pad.kanthaus.online/come/download > "/tmp/item.md"
  coMeDate=$(cat /tmp/item.md | grep "date:" | cut -d \" -f2 | cut -d \" -f1 | tail -1)
  echo -n "Pulling kanthaus.online... "
  git -C "$repoPath" pull
  coMeDir="user/pages/40.governance/90.minutes"
  newCoMeDir="${repoPath}/${coMeDir}/${coMeDate}_CoMe"
  mkdir "${newCoMeDir}"
  mv /tmp/item.md "${newCoMeDir}"
  echo ""
  printf "\e[1mNew CoMe page created! \e[0m"
  echo "Please check contents are accurate, as there have been issues in the past. E.g:"
  echo ""
  echo "  nano ${newCoMeDir}/item.md"
  echo ""
  printf "\e[1mOnce satisfied; \e[0m"  
  echo "add, commit then push the new page. E.g:"
  echo ""
  echo "  cd ${repoPath} && git add ${coMeDir}/${coMeDate}_CoMe/item.md && git commit -m \"uploading ${coMeDate} CoMe\" && git push && cd -"
  echo ""
else
  echo "See you when you're ready."
fi
