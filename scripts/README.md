# Here be scripts!

Almost all of them have 'help' information that can be found with `<command> -h`

To get an overview of what all the scripts do, run `./scriptsOverview`

To make the scripts a bit easier to use (i.e. from any directory, with abbreviated command names), include the following somewhere in your shell initialization configuration chain (e.g. `~/.aliases` or `~/.bashrc`):

```
# Source kanthaus scripts aliases
scriptsPath="$HOME/kanthaus-governance/scripts"
if [ -f "$scriptsPath/.kanthausAliases" ]; then
    . "$scriptsPath/.kanthausAliases"
fi
```

(N.b.: your `kanthaus-governance` directory is assumed to be in your home directory. Change `scriptsPath` if it is something else!)

