#!/bin/bash

cd "$(dirname "$0")"
source lib/bash_functions.sh

startDate="2017-07-01"
todaysDate=$(date -I)

# Identify path in which this script is found
scriptPath=$(dirname "${BASH_SOURCE[0]}")
basePath=$(_identifyBasePath "kanthaus/kanthaus-private")

if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then

  echo "Usage: $(basename "$0") [-h] [-c]"
  echo ""
  echo "Graphs number of residents over time"
  echo ""
  echo "optional arguments:"
  echo "  -h, --help            show this help message and exit"
  echo "  -c, --compile         re-compile .resGraphData"
  exit 0
  
elif [ "$1" == "-c" ] || [ "$1" == "--compile" ]; then
  
  if [ -f "$basePath/.resGraphData" ]; then
    rm "$basePath/.resGraphData"
  fi

  stopDate=$(date -d "${todaysDate} - 7 days" '+%Y-%m-%d')
  countDate="$startDate"
  while [ "$countDate" != "$stopDate" ]; do
    echo "$countDate,x," >> "$basePath/.resGraphData"
    ttlPpl=$(cat "$basePath/residenceRecord.csv" | grep "$countDate" | tr -cd , | wc -c)
    if [ "$ttlPpl" -lt 2 ]; then
      ttlPpl=0
    fi
    sed -e "s/x/${ttlPpl}/" -i "$basePath/.resGraphData"
    countDate=$(date -d "${countDate} + 1 days" '+%Y-%m-%d')
  done

  avgStartDate=$(date -d "${startDate} + 7 days" '+%Y-%m-%d')
  avgStopDate="$stopDate"
  avgCountDate="$avgStartDate"
  while [ "$avgCountDate" != "$avgStopDate" ]; do
    ttlStartDate=$(date -d "${avgCountDate} - 7 days" '+%Y-%m-%d')
    ttlCountDate="$ttlStartDate"
    ttlStopDate="$avgCountDate"
    ttlSum=0
    while [ "$ttlCountDate" != "$ttlStopDate" ]; do
      ttlDay=$(grep "$ttlCountDate" "$basePath"/.resGraphData | cut -d "," -f2 | cut -d "," -f1)
      ttlSum=$((ttlSum + ttlDay))
      ttlCountDate=$(date -d "${ttlCountDate} + 1 days" '+%Y-%m-%d')
    done
    avg=$(echo "scale=1; $ttlSum / 7" | bc)
    sed -e "/${avgCountDate}/ s/$/${avg}/" -i "$basePath/.resGraphData"
    avgCountDate=$(date -d "${avgCountDate} + 1 days" '+%Y-%m-%d')
  done
  
else # no arguments

  if [ ! -e "$basePath/.resGraphData" ]; then
    printf "\e[1m.resGraphData not found:\e[0m run 'resgraph --compile' to generate graph data\n."
  elif command -v gnuplot &> /dev/null; then
    gnuplot --persist <<-EOFMarker
      set xdata time
      set timefmt "%Y-%m-%d"
      set format x "%Y-%m-%d"
      set format y "%.0f"
      set datafile separator ","
      set xrange ["$startDate":"$todaysDate"]
      set yrange [0:35]    
      set xlabel "{/:Bold Time (YYYY-MM)}"
      set ylabel "{/:Bold Recorded residents (No.)}"
      plot "$basePath/.resGraphData" using 1:2 with steps lw 5 title 'Number of resident', \
           "$basePath/.resGraphData" using 1:2 with fillsteps fs solid 0.3 noborder lt 1 notitle, \
           "$basePath/.resGraphData" using 1:3 with steps lw 5 title 'Moving average of today + last 6 days (1.dp)'
      pause mouse close
EOFMarker
  else
    printf "\e[1mGnuplot not installed\e[0m Install gnuplot, or plot %s/.resGraphData some other way\n." "$basePath"
  fi
fi
