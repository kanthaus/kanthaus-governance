#!/bin/bash

cd "$(dirname "$0")"
source lib/bash_functions.sh

if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
    echo "Usage: $(basename "$0") [-h]"
    echo ""
    echo "Opens the residence record in nano/vi/m at the line of todays date"
    echo ""
    echo "optional arguments:"
    echo "  -h, --help            show this help message and exit"
    exit 0
fi

# Identify path in which this script is found
scriptPath=$(dirname "${BASH_SOURCE[0]}")

basePath=$(_identifyBasePath "kanthaus/kanthaus-private")

echo -n "Pulling kanthaus-private... "
git -C "$basePath" pull
todaysLine=$(cat "$basePath/residenceRecord.csv" | grep -n "$(date -I)")
todaysLine=$(echo "$todaysLine" | cut -f1 -d":")

# Open residence record at given line
if command -v nano &> /dev/null; then
  nano +"$todaysLine" "$basePath/residenceRecord.csv"
elif command -v vim &> /dev/null; then
  vim +"$todaysLine" "$basePath/residenceRecord.csv"
else
  vi +"$todaysLine" "$basePath/residenceRecord.csv"
fi