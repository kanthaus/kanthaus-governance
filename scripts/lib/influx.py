import http.client
import json
from urllib.parse import urlparse, quote
import urllib.request
from lib.kanthaus_private import credentials

"""Indeces of the three temperaturs measured in get_outdoor_temperature()[i]"""
OUTDOOR_TEMPERATURE_INDEX_MIN = 0
OUTDOOR_TEMPERATURE_INDEX_MAX = 2
OUTDOOR_TEMPERATURE_INDEX_AVG = 1

def influxquery(query):
    req = urllib.request.Request(
        url="https://influxdb.kanthaus.online:443/query?db=kanthaus&q=" + quote(query) + "&epoch=ms",
        headers={
            'Authorization': credentials['influxdb_auth_reader']
        }
    )
    u = urllib.request.urlopen(req)
    return json.load(u)

def influxwrite(data):
    req = urllib.request.Request(
        url="https://influxdb.kanthaus.online:443/write?db=kanthaus",
        headers={
            'Authorization': credentials['influxdb_auth_writer']
        }
    )
    urllib.request.urlopen(req,
        data=data.encode('utf8')
    )

def get_electricity_import_absolute(lower, upper):
    res = influxquery(
        """
            SELECT (last("value") - first("value")) * 0.1
            FROM "homeautomation.Obis"
            WHERE time >= '{}Z' AND time <= '{}Z' AND "node_id" = '4' AND "code"='1.8.0'
        """.format(lower, upper)
    )

    return res["results"][0]["series"][0]["values"][0][1]


def get_heating_electricity_10s(days=7, offset=0):
    res = influxquery(
        """
            SELECT difference(last("total_energy_mwh")) / 1000
            FROM "housebus.electricity.meter.1.0"
            WHERE time >= now() - {}d AND time <= now() - {}d AND "node_id" = '31' AND "port"='1010'
            GROUP BY time(10s) fill(none)
        """.format(days + offset, offset)
    )

    return res["results"][0]["series"][0]["values"]

def get_heating_dhw_status_10s(days=7, offset=0):
    res = influxquery(
        """
            SELECT max(valve_position_dhw) 
            FROM "housebus.heating.heating_status.1.0"
            WHERE time >= now() - {}d AND time <= now() - {}d
            GROUP BY time(10s) fill(none)
        """.format(days + offset, offset)
    )

    return res["results"][0]["series"][0]["values"]

def get_electricity_import_10s(days=7, offset=0):
    res = influxquery(
        """
            SELECT difference(last("value")) / 10
            FROM "homeautomation.Obis"
            WHERE time >= now() - {}d AND time <= now() - {}d AND "node_id" = '4' AND "code"='1.8.0'
            GROUP BY time(10s) fill(none)
        """.format(days + offset, offset)
    )

    return res["results"][0]["series"][0]["values"]

def get_k20_3_hp_electricity_10s(days=7, offset=0):
    res = influxquery(
        """
            SELECT non_negative_difference(last("energy_wh"))
            FROM "nous-3-k20-3-aircon"
            WHERE time >= now() - {}d AND time <= now() - {}d
            GROUP BY time(10s) fill(none)
        """.format(days + offset, offset)
    )

    return res["results"][0]["series"][0]["values"]

def get_electricity_import(days=7, offset=0):
    res = influxquery(
        """
            SELECT (last("value") - first("value")) * 0.1
            FROM "homeautomation.Obis"
            WHERE time >= now() - {}d AND time <= now() - {}d AND "node_id" = '4' AND "code"='1.8.0'
        """.format(days + offset, offset)
    )

    return res["results"][0]["series"][0]["values"][0][1]

def get_electricity_export(days=7, offset=0):
    res = influxquery(
        """
            SELECT (last("value") - first("value")) * 0.1
            FROM "homeautomation.Obis"
            WHERE time >= now() - {}d AND time <= now() - {}d AND "node_id" = '4' AND "code"='2.8.0'
        """.format(days + offset, offset)
    )

    return res["results"][0]["series"][0]["values"][0][1]

def get_electricity_production(days=7, offset=0):
    sum = 0
    res = influxquery(
        """
            SELECT last("total_wh")-first("total_wh")
            FROM "modbus"
            WHERE time >= now() - {}d AND time <= now() - {}d
            GROUP BY "name"
        """.format(days + offset, offset)
    )
    for i in range(len(res['results'][0]['series'])):
        sum += res['results'][0]['series'][i]['values'][0][1]


    res_stp10a = influxquery(
        """
            SELECT integral("dc_power_a", 1h)
            FROM "modbus"
            WHERE time >= now() - {}d AND time <= now() - {}d AND ac_power > 0 AND "name"='STP10'
            fill(none)
        """.format(days + offset, offset)
    )
    STP10_a = res_stp10a["results"][0]["series"][0]['values'][0][1]
    estimated_K22W_OL = STP10_a / 1.5
    sum += estimated_K22W_OL
    return sum


def get_outdoor_temperature(days=7, offset=0):
    res = influxquery(
        """
            SELECT min("temperature") / 100, mean("temperature") / 100, max("temperature") / 100
            FROM "homeautomation.Environment"
            WHERE ("node_id" = '5')
            AND time >= now() - {}d AND time <= now() - {}d
        """.format(days + offset, offset)
    )

    return res["results"][0]["series"][0]["values"][0][1:4]

def get_water_usage(days=7, offset=0):
    res = influxquery(
        """
            SELECT (last("main/value") - first("main/value"))
            FROM "main_water_meter"
            WHERE time >= now() - {}d AND time <= now() - {}d
        """.format(days + offset, offset)
    )

    if 'series' in res['results'][0]:
        return res["results"][0]["series"][0]["values"][0][1]
    else:
        return 0


