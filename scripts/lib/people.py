from datetime import datetime, timedelta
from math import inf

from lib import thresholds
from lib.base import EvaluationCriteria, Position, PositionMeta, Person, EvaluationType
from lib.evaluations import get_evaluation_data
from lib.residence import get_residence_data
from lib.utils import last, group_dates


VISIT_EXPIRATION_DAYS = 180


def get_evaluation_reasons(
    position,
    days_visited_without_evalution,
    days_since_last_evaluation,
    days_absent,
):
    """Return a list of ALL possible evaluation criteria

    To see if the criteria is active, check the "required" property on the EvaluationCriteria
    """
    return [
        EvaluationCriteria(
            type=EvaluationType.DAYS_VISITED,
            value=days_visited_without_evalution,
            threshold=thresholds.DAYS_VISITED[position],
            required=days_visited_without_evalution > thresholds.DAYS_VISITED[position],
        ),
        EvaluationCriteria(
            type=EvaluationType.ABSENCE,
            value=days_absent,
            threshold=thresholds.ABSENCE[position],
            required=days_absent > thresholds.ABSENCE[position],
        ),
        EvaluationCriteria(
            type=EvaluationType.ABSOLUTE_DAYS,
            value=days_since_last_evaluation,
            threshold=thresholds.ABSOLUTE_DAYS[position],
            required=days_since_last_evaluation > thresholds.ABSOLUTE_DAYS[position],
        ),
    ]


def create_position_meta(dates, evaluations):
    """From a list of dates and evaluations returns information about the position"""
    position = Position.VISITOR
    days_since_last_evaluation = inf
    days_absent = inf

    last_evaluation = last(evaluations)
    last_visit = last(dates)

    # Ignore visits that are more than VISIT_EXPIRATION_DAYS in the past
    # I believe that is the least we can do to fulfil §5a (8.1) from the Kanthaus constitution:
    # "A person ceases to be a Visitor if they leave the space"
    expiration_date = (datetime.now() - timedelta(days=VISIT_EXPIRATION_DAYS))

    if last_evaluation:
        position = last_evaluation.NewPosition
        min_date = last_evaluation.Date

        if position == Position.VISITOR:
            # If they became Visitor again, only count visits after the last evaluation and VISIT_EXPIRATION_DAYS
            min_date = max(expiration_date, min_date)

        days_visited_without_evalution = len([date for date in dates if min_date < date <= datetime.now()])

        days_since_last_evaluation = (datetime.now() - last_evaluation.Date).days
    else:
        days_visited_without_evalution = len([date for date in dates if expiration_date < date <= datetime.now() ])

    if last_visit:
        days_absent = (datetime.now() - last_visit).days

    reasons = get_evaluation_reasons(position, days_visited_without_evalution, days_since_last_evaluation, days_absent)

    return PositionMeta(position, reasons)


def get_people():
    """Get a list of People with all the interesting data inside:

    the elements are of type Person, defined in lib.base:
        name: the Name as in residence record and evaluation record
        dates: the dates when the person was in Kanthaus (according to residence record)
        visits: list of time ranges (as [start, end]) when the person was in Kanthaus
        evaluations: data about the evaluations, data structure details see lib.evaluations
        meta: a namedtuple (see lib.base.PositionMeta) with the current position and the reason for this position

    data structure:
        get_people()[0, ...].name -> str
        get_people()[0, ...].dates[0, ...] -> datetime (day when person was there)
        get_people()[0, ...].visits[0, ...][0] -> datetime (start date)
        get_people()[0, ...].visits[0, ...][1] -> datetime (end date)
        get_people()[0, ...].evaluations[0, ...][one of lib.evaluations.FIELD_NAMES] -> str/ datetime
        get_people()[0, ...].meta.position -> Enum, see lib.base.Position
        get_people()[0, ...].meta.reasons[0, 1, 2] -> see lib.base.EvaluationCriteria
    """

    evaluations = get_evaluation_data()
    residence = get_residence_data()

    resident_names = set(residence.keys())
    evaluated_names = set(evaluations.keys())

    names_evaluated_but_not_resident = evaluated_names - resident_names
    if names_evaluated_but_not_resident:
        raise ValueError("The following names appear in the evaluation record, but not in the residency record: "+str(names_evaluated_but_not_resident))

    return [
        Person(
            name=name,
            dates=residence[name],
            visits=group_dates(residence[name]),
            evaluations=evaluations[name],
            meta=create_position_meta(
                residence[name],
                evaluations[name],
            ),
        ) for name in sorted(resident_names)
    ]
