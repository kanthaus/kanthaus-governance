from datetime import datetime, timedelta

from lib.base import EvaluationType
from lib.utils import unidecode
from math import inf


def filter_by_name(texts):

    def normalize(val):
        return unidecode(val.lower())

    normalized_texts = [normalize(val) for val in texts]

    def f(person):
        return any([text in normalize(person.name) for text in normalized_texts])

    return f


def filter_by_recent(days):
    recent = datetime.now() - timedelta(days=days)

    def f(person):
        return any([date for date in person.dates if date >= recent])

    return f


def filter_by_year(year):
    lower = datetime(year=year, month=1, day=1)
    upper = datetime(year=year + 1, month=1, day=1)

    def f(person):
        return any([date for date in person.dates if lower < date < upper])

    return f


def evaluation_is_soon(reason, days):
    if reason.required:
        return False
    return (reason.threshold - reason.value) < days


def filter_reason(reason, due, soon, soon_days):
    if due and reason.required:
        return True
    elif soon and evaluation_is_soon(reason, soon_days):
        return True
    return False


def filter_by_evaluation(due=False, soon=False, soon_days=7):

    def f(person):
        return any([filter_reason(reason, due, soon, soon_days) for reason in person.meta.reasons])

    return f


def filter_by_absent_reason():

    def f(person):
        return any(
            [reason for reason in person.meta.reasons if reason.required and reason.type == EvaluationType.ABSENCE]
        )

    return f


def how_overdue(person):
    """Returns how overdue the evaluation of a person is.

    This is done by given the percentage of days the evaluation is over due in relation to
    the number of days between evaluations for this person.
    """
    # error if person.meta.reasons is empty list
    # but then something is wrong and we should get an error
    return max([(reason.value - reason.threshold) / reason.threshold if reason.threshold != inf else -inf for reason in person.meta.reasons])

def how_overdue_in_days(person):
    """Returns how overdue the evaluation of a person is, in number of days to the evaluation, for people
    to be evaluated in the future.
    """
    # error if person.meta.reasons is empty list
    # but then something is wrong and we should get an error
    return min([reason.threshold - reason.value for reason in person.meta.reasons])
