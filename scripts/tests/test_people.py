import unittest
from datetime import timedelta

from lib import thresholds
from lib.base import Position, EvaluationType
from lib.evaluations import Evaluation
from lib.people import create_position_meta
from lib.utils import now_date


def create_date_range(start_date, days):
    return [start_date + timedelta(days=n) for n in range(days)]


class TestPositionMeta(unittest.TestCase):

    def test_is_a_visitor_without_any_evaluations(self):
        now = now_date()
        meta = create_position_meta(
            dates=[now, now + timedelta(days=1)],
            evaluations=[],
        )
        self.assertEqual(meta.position, Position.VISITOR)

    def test_becomes_a_volunteer_after_an_evaluation(self):
        now = now_date()
        meta = create_position_meta(
            dates=[now, now + timedelta(days=1)],
            evaluations=[
                Evaluation(
                    Date=now,
                    Person='foo',
                    PreviousPosition=Position.VISITOR,
                    AppliedFor=Position.VOLUNTEER,
                    NewPosition=Position.VOLUNTEER,
                ),
            ],
        )
        self.assertEqual(meta.position, Position.VOLUNTEER)

    def test_visitor_evaluation_after_some_time(self):
        date_count = 25
        start_date = now_date() - timedelta(weeks=4)
        meta = create_position_meta(
            dates=create_date_range(start_date, date_count),
            evaluations=[],
        )
        self.assertEqual(meta.position, Position.VISITOR)

        required_reasons = [reason for reason in meta.reasons if reason.required]
        self.assertEqual(len(required_reasons), 1)
        criteria = required_reasons[0]
        self.assertEqual(criteria.type, EvaluationType.DAYS_VISITED)
        self.assertEqual(criteria.value, date_count)
        self.assertEqual(criteria.threshold, thresholds.DAYS_VISITED[meta.position])

    def test_volunteer_evaluation_due_after_cumulative_days(self):
        last_evaluation_date = now_date() - timedelta(days=180)
        meta = create_position_meta(
            dates=[
                *create_date_range(now_date() - timedelta(days=180), 30),
                *create_date_range(now_date() - timedelta(days=32), 32),
            ],
            evaluations=[
                Evaluation(
                    Date=last_evaluation_date,
                    Person='foo',
                    PreviousPosition=Position.VISITOR,
                    AppliedFor=Position.VOLUNTEER,
                    NewPosition=Position.VOLUNTEER,
                ),
            ],
        )
        self.assertEqual(meta.position, Position.VOLUNTEER)

        required_reasons = [reason for reason in meta.reasons if reason.required]
        self.assertEqual(len(required_reasons), 1)
        criteria = required_reasons[0]
        self.assertEqual(criteria.type, EvaluationType.DAYS_VISITED)
        self.assertEqual(criteria.value, 61)
        self.assertEqual(criteria.threshold, thresholds.DAYS_VISITED[meta.position])

    def test_volunteer_evaluation_due_after_absolute_days(self):
        last_evaluation_date = now_date() - timedelta(days=367)
        meta = create_position_meta(
            dates=create_date_range(now_date() - timedelta(days=35), 30),
            evaluations=[
                Evaluation(
                    Date=last_evaluation_date,
                    Person='foo',
                    PreviousPosition=Position.VISITOR,
                    AppliedFor=Position.VOLUNTEER,
                    NewPosition=Position.VOLUNTEER,
                ),
            ],
        )
        self.assertEqual(meta.position, Position.VOLUNTEER)
 
        required_reasons = [reason for reason in meta.reasons if reason.required]
        self.assertEqual(len(required_reasons), 1)
        criteria = required_reasons[0]
        self.assertEqual(criteria.type, EvaluationType.ABSOLUTE_DAYS)
        self.assertEqual(criteria.value, 367)
        self.assertEqual(criteria.threshold, thresholds.ABSOLUTE_DAYS[meta.position])

    def test_member_evaluation_due_after_cumulative_days(self):
        last_evaluation_date = now_date() - timedelta(days=250)
        meta = create_position_meta(
            dates=[
                *create_date_range(now_date() - timedelta(days=250), 100),
                *create_date_range(now_date() - timedelta(days=82), 82),
            ],
            evaluations=[
                Evaluation(
                    Date=last_evaluation_date,
                    Person='foo',
                    PreviousPosition=Position.VOLUNTEER,
                    AppliedFor=Position.MEMBER,
                    NewPosition=Position.MEMBER,
                ),
            ],
        )
        self.assertEqual(meta.position, Position.MEMBER)

        required_reasons = [reason for reason in meta.reasons if reason.required]
        self.assertEqual(len(required_reasons), 1)
        criteria = required_reasons[0]
        self.assertEqual(criteria.type, EvaluationType.DAYS_VISITED)
        self.assertEqual(criteria.value, 181)
        self.assertEqual(criteria.threshold, thresholds.DAYS_VISITED[meta.position])

    def test_member_evaluation_due_after_absolute_days(self):
        last_evaluation_date = now_date() - timedelta(days=367)
        meta = create_position_meta(
            dates=create_date_range(now_date() - timedelta(days=35), 30),
            evaluations=[
                Evaluation(
                    Date=last_evaluation_date,
                    Person='foo',
                    PreviousPosition=Position.VOLUNTEER,
                    AppliedFor=Position.MEMBER,
                    NewPosition=Position.MEMBER,
                ),
            ],
        )
        self.assertEqual(meta.position, Position.MEMBER)
 
        required_reasons = [reason for reason in meta.reasons if reason.required]
        self.assertEqual(len(required_reasons), 1)
        criteria = required_reasons[0]
        self.assertEqual(criteria.type, EvaluationType.ABSOLUTE_DAYS)
        self.assertEqual(criteria.value, 367)
        self.assertEqual(criteria.threshold, thresholds.ABSOLUTE_DAYS[meta.position])

    def test_no_evaluation_of_dependents(self):
        now = now_date()
        date_count = 25
        start_date = now_date() - timedelta(weeks=4)
        meta = create_position_meta(
            dates=create_date_range(start_date, date_count),
            evaluations=[
                Evaluation(
                    Date=now,
                    Person='foo',
                    PreviousPosition=Position.VISITOR,
                    AppliedFor=Position.DEPENDENT,
                    NewPosition=Position.DEPENDENT,
                ),
            ],
        )
        self.assertEqual(meta.position, Position.DEPENDENT)

        required_reasons = [reason for reason in meta.reasons if reason.required]
        self.assertEqual(required_reasons, [])
