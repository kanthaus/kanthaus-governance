import unittest
from lib.utils import group_dates
from datetime import datetime, timedelta

class TestUtils(unittest.TestCase):
    
    def test_nodates_nogroups(self):
        """Test if an empty dates list gives an empty list of date groups."""
        self.assertEqual(group_dates([]), [])
    
    def test_singledate(self):
        """Test if a single date is correctly grouped as one group."""
        aDay = datetime(2020, 2, 2)
        self.assertEqual(group_dates([aDay]),
                         [[aDay, aDay]])
        
    def test_twodategroups(self):
        """Test if two date groups are recognized correctly."""
        aDay = datetime(2020, 2, 2)
        dates = [aDay]
        for days in range(1, 5):
            dates.append(aDay + timedelta(days=days))
        for days in range(8, 12):
            dates.append(aDay + timedelta(days=days))
        self.assertEquals(group_dates(dates),
                          [[aDay, aDay + timedelta(days=4)],
                           [aDay + timedelta(days=8),
                            aDay + timedelta(days=11)]]
                          )
    
    def test_unorderedDates(self):
        """Test for inverse chronological order.
        
        groupDates should produce just one group
        if dates are going from future to past.
        """
        aDay = datetime(2020, 2, 2)
        dates = [aDay - timedelta(days=d) for d in range(5)]
        dategroups = group_dates(dates)
        self.assertEqual(len(dategroups), 1)
        self.assertLess(dategroups[0][1], dategroups[0][0])
        