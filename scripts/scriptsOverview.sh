#!/bin/bash

scriptPath=$(dirname "${BASH_SOURCE[0]}")

if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
  echo "Usage: $(basename "$0") [-h] [-c]"
  echo ""
  echo "Overview of scripts based on help descriptions"
  echo ""
  echo "optional arguments:"
  echo "  -h, --help            show this help message and exit"
  echo "  -c, --compile         re-compile .scriptsOverview"
  exit 0
elif [ "$1" == "-c" ] || [ "$1" == "--compile" ]; then
  cd "$scriptPath" || exit
  echo -n "Pulling changes..."
  git pull
  if [ -a "$scriptPath/.scriptsOverview" ]; then
    rm "$scriptPath"/.scriptsOverview
  fi
  
  echo "Compiling scripts overview..."
  for script in * ; do
    if [ -x "$script" ] && [ ! -d "$script" ] && [ "$script" != "test" ]; then
      echo -n "$script... "
      
      echo -n "$script£" >> "$scriptPath/.scriptsOverview"
      
      scriptAlias=$(grep "$script\"" "$scriptPath"/.kanthausAliases | cut -d " " -f2 | cut -d "=" -f1 | tr -d '\n')
      if [ -n "$scriptAlias" ]; then
        echo -n "$scriptAlias£" >> "$scriptPath"/.scriptsOverview
      else
        echo -n "(none)£" >> "$scriptPath"/.scriptsOverview
      fi
      
      scriptHelp=$("$scriptPath/$script" -h | head -3 | tail -1)
      if [ -n "$scriptHelp" ]; then
        echo "$scriptHelp" >> "$scriptPath"/.scriptsOverview
      else
        echo "(none)" >> "$scriptPath"/.scriptsOverview
      fi
      
      echo "done"
    fi
  done
  printf "\e[1mOverview compiled.\e[0m\n" 

else

  column -t -W 3 -s "£" --table-columns SCRIPT,ALIAS,DESCRIPTION "$scriptPath/.scriptsOverview"
  
fi
