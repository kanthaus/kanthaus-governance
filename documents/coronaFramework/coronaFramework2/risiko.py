import numpy as np
import matplotlib
import matplotlib.pyplot as plt

contacts = ["many (>10)", "few (3-10)", "(nearly) none (0-2)"]
region = ["low\n< 50", "medium\n≤100", "high\n> 100\n(or contact travels a lot)"]

#risk = np.array([[5, 10, 10],
                    #[2, 5, 8],
                    #[1, 1, 2]])

risk = np.array([[4, 5, 5],
                 [2, 4, 5],
                 [1, 1, 2]])

fig, ax = plt.subplots(figsize=(7.5, 5.5))
im = ax.imshow(risk, cmap='OrRd')

# We want to show all ticks...
ax.set_xticks(np.arange(len(region)))
ax.set_yticks(np.arange(len(contacts)))
# ... and label them with the respective list entries
ax.set_xticklabels(region)
ax.set_yticklabels(contacts)

ax.set_xlabel('New cases in regions per 100.000 inhabitants (7 days)\n(Highest region of the contacts you meet)')
ax.set_ylabel('Number of contacts of the contact(s) you meet (14 days)\n')

# Rotate the tick labels and set their alignment.
#plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
         #rotation_mode="anchor")

# Loop over data dimensions and create text annotations.
for i in range(len(contacts)):
    for j in range(len(region)):
        text = ax.text(j, i, risk[i, j],
                       ha="center", va="center", color="black", size="40")

ax.set_title("Probability of a contact having corona")

# Add table number (to ease reference from the framework text)
plt.figtext(0.05, 0.05, '1', horizontalalignment='left', size='25')

fig.tight_layout()
plt.savefig('risk.svg')
plt.show()

