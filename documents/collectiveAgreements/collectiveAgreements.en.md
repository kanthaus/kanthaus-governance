# Collective Agreements


### Weekly meeting cycle

#### Coordination Meeting (CoMe)

By default we meet on Mondays at 10:00 to distribute information, make plans for the week and generally coordinate for maximum one hour. All residents are recommended to join.

===

#### Power Hour

Right after CoMe, Mondays at 11:00, we briefly plan what communal cleaning needs to be done, and then clean for at least an hour. This is in addition to ordinary cleaning, not a replacement. All residents are recommended to join.

### Monthly meeting cycle

By default the following events take place once every four weeks on Fridays at 10:00. The 4th week is left without recurring appointment on purpose.

#### Project Updates

We give space for people to briefly present what they're currently working at. It's about sharing ideas and celebrating achievements, and about strengthening the project focus of the house. All residents are invited to join and to present.

#### Social Sauna

We hold a session to surface social tensions and conflicts in the house. It's not necessarily about solving problems, but giving residents the opportunity to speak out loud anything that bothers them and affects community life. Visitors are not invited. Volunteers and members are recommended to join.

#### Planning Meeting (PlaMe)

Every topic that is too big for CoMe and/or is relevant to more than just the current week can be discussed in PlaMe. In this meeting decisions can be made. All residents are invited to join, volunteers and members are recommended to join. Remote participation is encouraged.

### English as default language
We are an international project and want non-German speakers to be part of our everyday life. English is therefore our default, common language both online and in-house. German will necessarily be used on bureaucratic or technical grounds: do ask for a translation if you are interested and weren't offered. We love all other languages, but ask speakers to remain aware they are usually more exclusive. In our day-to-day communication we use a lot of creative language mixes (Denglish, Frenglish) and are happy for this to continue :)

### Hosting Duty
Hosts—Volunteers or Members which agree to host a Visitor—are responsible for taking care that their Visitor and the other residents have the best time possible. This responsibility includes tour-guiding, question answering, helping to avoid common problems and resolving tensions. Hosts are also responsible for organizing a check-in for the Visitor after the Visitor has spent ~7 days at Kanthaus. The check-in round is an informal chat in which the Visitor, their host and one or two Volunteers/Members see how things are going for the Visitor and other residents. Hosting Duty should be explicitly transferred if the current host leaves for more than 2 or 3 days.

### Quiet Hours
We want everyone to be able to sleep well. A general guideline is to be as quiet as possible between 00:00 and 07:00, especially (near to) where people sleep K20-2 and K22-2. Only do loud things between 09:00 and 21:00. Significant deviations to this guideline should be proposed in advance, preferably at the Coordination Meeting.

### Clear Table Policy
Please help keep the shared table space as usable as possible. In general, clear space as soon as you are finished. If you need to leave things on a table, try do so in a way that leaves as much space for others (e.g. stack papers on top of your laptop.) The living room and main kitchen tables should always be left clear: don't leave these table with anything on them and wipe unless very clean.

### Clear Windowsill Policy
Every room needs at least one window that can be easily opened: please do not leave anything on the windowsill that is currently clear.

### Don't break the windows
Please use the window holders. If there isn't a window holder or if it's not possible to use it, don't leave it unattended or find another way to prevent it from slamming.

### Smoking
Do not smoke at all within the buildings. If you smoke outside, please ensure that smoke does not come in through open doors or windows. Do not throw cigarette butts on the ground! They can be disposed of in the black bin in the garden.
We agree that incense may only be used in certain, designated rooms.

### Alcohol
Kanthaus is not a bar: please do not come here with drinking as a primary purpose and be aware that alcohol consumption will not be considered as an excuse or justification of behavior. That said, responsible alcohol consumption is tolerated. Please contact a Volunteer or Member with concerns or questions regarding this topic.

### Animals
Animals are not necessarily welcome due to factors such as: allergic reactions, hair-shedding, defecation, damage to resources, biting and other undesirable behaviors. Potential animal residents will be considered on a case by case basis. Please contact a Member to discuss bringing an animal.

### Changes to the Collective Agreements
Any addition, subtraction or other change to this document should be made according to the [Constitution](../constitution#collagrchange).
