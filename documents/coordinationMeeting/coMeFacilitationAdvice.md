# CoMe facilitation advice

## About the CoMe
The **Co**ordination **Me**eting is the only weekly meeting everyone is [recommended](https://kanthaus.online/en/governance/collectiveagreements) to join. This makes it the main 'real-life' event we have for sharing information and ideas. We try and keep it short, and as relevant for the participants as possible. In consequence, it can be quite rapid: it's not a good place for resolving emotional conflicts or making complex decisions.

## Requirements
- a Kanthaus [nextcloud](https://cloud.kanthaus.online) account with membership of the `Kanthaus` group
- a [yunity slack](https://yunity.slack.com/) account with access to the private `kanthaus-residence` channel
- a Kanthaus [gitlab](https://gitlab.com/kanthaus) account with at least `Maintainer` permissions
- to know where the cash box is and how to open it

## Preparation

### Check dates
Ensure that both meta-data date and markup date are current.

### Residence record
Check that the [residence record](https://gitlab.com/kanthaus/kanthaus-private/-/blob/master/residenceRecord.csv) is up to date.

### Run the script
After you have updated the records, go to the [pipeline schedules](https://gitlab.com/kanthaus/kanthaus-governance/pipeline_schedules) and run the `notify slack about position evaluations` pipeline by pressing the 'play' button.
- After some minutes, the bot will post data to the `kanthaus-residence` channel.
- Copy the contents in the gray 'stats' box to the `Previous week stats` CoMe section

### Trash
Check the dates for trash pickups in the [Kanthaus calendar](https://cloud.kanthaus.online/apps/calendar/dayGridMonth/now) and write them down in the `Schedule` section. Make sure to include any trash pickup on the next monday, as next CoMe will be to late to plan for it.

### Money
Check the donation 'shoe' in the k20 hallway and deposit it in the cash box. List this sum plus any other donation in the last week in the `Income` CoMe section. If you're feeling energetic, you can also check the donation box in the Verschenkeladen.

### Evaluations
Included in the post you sent to `kanthaus-residence` earlier are any evaluation notices. Copy `Due for evaluation` and `Due for evaluation soon` entries to the `Evaluations and check-ins` CoMe section

## During
There are no fixed rules, so feel free to add your own touch and do (small) experiments. Some general advice that seems to have worked so far:
- Notify people 10 minutes before the meeting starts.
- Start as close to 10:00 as possible.
- Go through the ordered points in order, even if nothing has changed.
- At the end of each section, ask people if they have anything add (and give them some moments to think!)
- Do moderate discussion if off-topic or too detailed. You can always suggest a small group forms for special topics.
- Try to finish the meeting before 11:00. If you think it really makes sense to extend it, announce that.
- Try to leave the room as soon as CoMe has has ended. (This sends a signal to everyone else that they can also leave if they want to.)

## Afterwards
'Sanitize' the CoMe pad by removing any data which shouldn't be published. This includes anything marked `to be removed` or `tbr`, as well as any personal information about people who haven't agreed to it.

Copy CoMe pad contents to the website. A graphical way of doing this can be done by:
- Go to the [minutes](https://gitlab.com/kanthaus/kanthaus.online/-/tree/master/user/pages/40.governance/90.minutes) folder
- Click `+` -> `New file`
- As filename use `YYYY-MM-DD_CoMe/item.md` (e.g. `2023-05-15_CoMe/item.md`)
- Copy the following metadata header (including the `---`) into this new document, and fix the date:
```
---
title: Coordination Meeting
date: "2023-MM-DD"
taxonomy:
    tag: [come]
---
```
- Copy the CoMe pad content from the Edit mode/Bearbeiten Modus (black window) after the metadata header.
- Change the commit message to something like `add CoMe minutes for YYYY-MM-DD`
- Click `Commit changes`
- That´s it! After a few minutes the CoMe-minutes should appear on the website.

Prepare the CoMe pad for next week
- clear all information that is no longer relevant (i.e. most of it except regular schedule events)
- put forward the meta-data date and markup date to next CoMe

Then, finally, notify #kanthaus that the minutes are online and pad is cleared. _Well done!_
