<figure style="font-family: serif">

<h3 style="text-align:center; padding: 0 20px 0 20px">Tabelle 3.1. <i>Bauprinzipien langlebiger Allmenderessourcen-Institutionen</i></h3>

<hr style="border: 0; border-top: 3px double #8c8c8c; background-color: transparent">

1. _Klar definierte Grenzen_  
	Die Personen oder Haushalte, die das Recht zur Entnahme von Ressourceneinheiten aus der AR haben, müssen, genauso wie die Grenzen der AR selbst, klar definiert sein.
	
2. _Kongruenz zwischen Aneignungs- und Bereitstellungsregeln und lokalen Bedingungen_  
	Aneignungsregeln, die Zeit, Ort, Technik und/oder Menge der Ressourceneinheiten beschränken, sind abgestimmt auf lokale Bedingungen und Bereitstellungsregeln, die ein bestimmtes Quantum an Zeit, Arbeit, Materialien und/oder Geld erfordern.
	
3. _Arrangements für kollektive Entscheidungen_  
	Die meisten Personen, die von den operativen Regeln betroffen sind, können über Änderungen der operativen Regeln mitbestimmen.
	
4. _Überwachung_  
	Die Überwacher, die aktiv den AR-Zustand und das Verhalten der Aneigner kontrollieren, sind den Aneignern gegenüber rechenschaftspflichtig oder sind selbst die Aneigner.
	
5. _Abgestufte Sanktionen_  
	Aneigner, die operative Regeln verletzen, werden von anderen Aneignern, von deren Bevollmächtigten oder von beiden glaubhaft mit abgestuften Sanktionen belegt (entsprechend der Schwere und dem Kontext des Vergehens).
	
6. _Konfliktlösungsmechanismen_  
	Die Aneigner und ihre Bevollmächtigten haben raschen Zugang zu kostengünstigen lokalen Arenen, die Konflikte zwischen Aneignern oder zwischen Aneignern und ihren Bevollmächtigten schlichten.
	
7. _Minimale Anerkennung des Organisationsrechts_  
	Das Recht der Aneigner, ihre eigenen Institutionen zu entwickeln, wird von keiner externen staatlichen Behörde in Frage gestellt.

_Für ARs, die Teile größerer Systeme sind:_

8. _Eingebettete Unternehmen_  
	Aneignung, Bereitstellung, Überwachung, Durchsetzung, Konfliktlösung und Verwaltungsaktivitaten sind in Unternehmen, die in mehrere Ebenen eingebettet sind, organisiert.

<hr style="border: 0; border-top: 3px double #8c8c8c; background-color: transparent">

</figure>

<small>Ostrom, Elinor (1990). _Die Verfassung der Allmende: Jenseits Von Staat Und Markt_ Mohr Siebeck; 1. Edition (1. März 1999) p117. ISBN 978-3161471360.</small>
